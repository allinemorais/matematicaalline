package com.NumerosRomanos.NumerosRomanos;

import java.util.HashMap;
import java.util.Map;

public class NumerosControler {
    private Map<String, Integer> mapaRomanosArabicos;

    public boolean converter(int numero)
    {
       this.RetorarRomano(numero);

        return false;
    }

    public String RetorarRomano(int numero)
    {
        mapaRomanosArabicos = new HashMap<String, Integer>();
        mapaRomanosArabicos.put("I", 1);
        mapaRomanosArabicos.put("II", 2);
        mapaRomanosArabicos.put("III", 3);
        mapaRomanosArabicos.put("IV", 4);
        mapaRomanosArabicos.put("V", 5);
        mapaRomanosArabicos.put("VI", 6);
        mapaRomanosArabicos.put("VII", 7);
        mapaRomanosArabicos.put("VIII", 8);
        mapaRomanosArabicos.put("IX", 9);
        mapaRomanosArabicos.put("X", 10);
        mapaRomanosArabicos.put("L", 50);
        mapaRomanosArabicos.put("C", 100);
        mapaRomanosArabicos.put("D", 500);
        mapaRomanosArabicos.put("M", 1000);

        return  "";
    }



}
