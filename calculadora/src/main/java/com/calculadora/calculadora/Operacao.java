package com.calculadora.calculadora;

import java.util.ArrayList;
import java.util.List;

public class Operacao {
    public static int Soma(int num1, int num2) {
        return  num1 + num2;
    }


    public static int Subtrair(int num1, int num2) {
        return  num1 - num2;
    }

    public static int SubtrairLista(ArrayList<Integer> numeros) {
        int resultado = 0;

        for (Integer numero: numeros)
        {
            resultado -= numero;
        }

        return resultado;
    }


    public static Integer Divisao(int num1, int num2) {
        int resultado = 0;
        resultado = num1 / num2;
        return resultado;
    }

    public static Integer Multiplicacao(int num1, int num2) {
        return num1 * num2;
    }

    public static Integer MultiplicarLista(ArrayList<Integer> numeros) {
        int resultado = 1;

        for (Integer numero: numeros) {
            resultado = numero * numero;
        }

        return resultado;
    }

    public static int SomarLista(ArrayList<Integer> numeros) {
        int resultado = 0;

        for (Integer numero: numeros) {
         resultado += numero;
        }

        return resultado;
    }
}
