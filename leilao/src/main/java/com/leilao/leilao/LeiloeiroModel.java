package com.leilao.leilao;

public class LeiloeiroModel {
    private String nome;
    private LeilaoModel leilao;

    public LeiloeiroModel() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LeilaoModel getLeilao() {
        return leilao;
    }

    public void setLeilao(LeilaoModel leilao) {
        this.leilao = leilao;
    }
}
