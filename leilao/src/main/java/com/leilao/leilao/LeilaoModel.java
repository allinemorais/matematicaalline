package com.leilao.leilao;

import ch.qos.logback.core.BasicStatusManager;

import java.util.ArrayList;
import java.util.List;

public class LeilaoModel {


    private ArrayList<LanceModel> lstLance = new ArrayList<>();

    public LeilaoModel(LanceModel lance) {
        lstLance.add(lance);
    }

    public LeilaoModel() {

    }


    public ArrayList<LanceModel> getLstLance() {
        return lstLance;
    }
}
