package com.leilao.leilao;

public class UsuarioMoldel {
    private int Id;
    private String nome;

    public UsuarioMoldel() {
       }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
