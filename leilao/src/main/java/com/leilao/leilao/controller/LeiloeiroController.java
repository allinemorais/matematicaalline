package com.leilao.leilao.controller;

import com.leilao.leilao.LanceModel;
import com.leilao.leilao.LeilaoModel;

public class LeiloeiroController {

    public static double RetornarMaiorLance(LeilaoModel leilaoModel) {
        double lanceAtual = 0;
        for (LanceModel lance: leilaoModel.getLstLance())
        {
            if(lance.getValorLance() > lanceAtual)
            {
                lanceAtual = lance.getValorLance();
            }
        }

        return lanceAtual;
    }
}
