package com.leilao.leilao;

import com.leilao.leilao.controller.LanceController;
import com.leilao.leilao.controller.UsuarioController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Lance {

    @Test
    public void SetarLance()
    {

        UsuarioMoldel usuarioModel = new UsuarioMoldel();
        LanceController lanceController = new LanceController();

        usuarioModel.setId(1);
        usuarioModel.setNome("julia");

        Assertions.assertEquals(lanceController.AdicionarLance(34.5, usuarioModel),true);
    }
}
